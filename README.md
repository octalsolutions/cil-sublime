# README #

This is a repository for enabling syntax highlighting for sublime text 3.

### License ###
MIT

### What is this repository for? ###

If you edit CIL in sublime, instead of this:
![Zrzut ekranu 2016-11-01 o 12.00.59.png](https://bitbucket.org/repo/rqLA79/images/1477416445-Zrzut%20ekranu%202016-11-01%20o%2012.00.59.png)

you will get this.
![Zrzut ekranu 2016-11-01 o 12.00.33.png](https://bitbucket.org/repo/rqLA79/images/4102216713-Zrzut%20ekranu%202016-11-01%20o%2012.00.33.png)

### How do I get set up? ###

* add this repository to sublime text (Add repository)
* install package CIL-sublime

### Contribution guidelines ###

* please contribute - this is an ongoing project

### Who do I talk to? ###

* Repo owner or admin